#ifndef SIGNALGRAPHPRINT_H
#define SIGNALGRAPHPRINT_H

#include <QThread>
#include <QPixmap>

class LQGraphWgt;

class LPlotSet;

/* вспомогательный класс для формирования изображения в отдельном потоке */
class LQGraphWgtPrint : public QThread
{
    Q_OBJECT
public:
    explicit LQGraphWgtPrint(const LQGraphWgt* graph, QObject *parent = 0);

protected:
    void run();
private:
    bool is_running;
    const LQGraphWgt *m_graph;

signals:
    void printDone(const unsigned char* buf, int width, int height);

public slots:
    void plot(int width, int height, int ch_cnt, LPlotSet **input_plots);
    void stop(void);
};

#endif // SIGNALGRAPHPRINT_H
