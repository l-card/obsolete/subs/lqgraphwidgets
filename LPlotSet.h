#ifndef LPLOTSET_H
#define LPLOTSET_H


#include <mgl2/mgl.h>


class LPlotSet
{
public:
    typedef enum
    {
        LPLOT_TYPE_SEQ = 0,
        LPLOT_TYPE_XY  = 1
    } LPlotType;



    mglData x;
    mglData y;
    LPlotType type;
    double dx;
    double offs;
    int ref_cnt;
};
#endif // LPLOTSET_H
