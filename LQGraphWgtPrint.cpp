#include "LQGraphWgtPrint.h"
#include "LQGraphWgt.h"
#include "mgl2/mgl.h"
#include <stdio.h>
#include "LPlotSet.h"

#include <QImage>

LQGraphWgtPrint::LQGraphWgtPrint(const LQGraphWgt* graph, QObject *parent) :
    m_graph(graph), QThread(parent), is_running(true)
{

}

void LQGraphWgtPrint::run()
{
    exec();
}

void LQGraphWgtPrint::stop()
{
    this->exit();
}


static mglData* show_dec(LPlotSet* src, mreal xmin, mreal xmax,int show_pts)
{
    int plot_pts = (xmax - xmin)/src->dx;
    mglData* dat = 0;

    if (show_pts && (plot_pts > 2*show_pts))
    {
        int n = plot_pts/show_pts;
        int x_size = src->y.GetNx();
        int show_size = x_size/n;


        dat = new mglData(show_size,1,1);
        for (int i=0; i < (show_size-1); i+=2)
        {
            mreal max, min, first, last;
            int pt=n*i;
            max=min=first = src->y.GetVal(pt);

            for (pt++ ; (pt < n*(i+2)) && (pt < x_size); pt++)
            {
                last = src->y.GetVal(pt);
                if (min > last)
                    min = last;
                if (max < last)
                    max = last;
            }

            if (first < last)
            {
                dat->SetVal(min, i);
                dat->SetVal(max, i+1);
            }
            else
            {
                dat->SetVal(max, i);
                dat->SetVal(min, i+1);
            }
        }

        if (show_size%2)
            dat->SetVal(src->y.GetVal(x_size-1), show_size-1);


    }
    return dat;
}

void LQGraphWgtPrint::plot(int width, int height, int ch_cnt, LPlotSet **input_plots)
{
    if (input_plots==0)
        return;
    int err = 0;
    //HMGL graph = mgl_create_graph(width, height);
    double xmin=0, xmax=0, ymin, ymax, xstep, ystep;
    mglGraph* gr = new mglGraph(0, width, height);

    ymin = m_graph->ymin();
    ymax = m_graph->ymax();
    xmin = m_graph->xmin();
    xmax = m_graph->xmax();
    xstep = m_graph->xstep();
    ystep = m_graph->ystep();

    LQGraphWgt::GraphMode mode = m_graph->mode();
    LPlotSet* plots = new LPlotSet[ch_cnt];
    bool* show_plots = new bool[ch_cnt];

    for (int ch=0; ch < ch_cnt; ch++)
    {
        if (input_plots[ch] && m_graph->plotIsEnabled(ch))
        {
            show_plots[ch] = true;
            plots[ch] = *input_plots[ch];
            if (mode==LQGraphWgt::MODE_HIST)
            {
                int points;
                mreal aymax = -RAND_MAX;
                mreal aymin = RAND_MAX;
                for (int pt=0; pt < plots[ch].y.GetNx(); pt++)
                {
                    if (aymin > plots[ch].y.a[pt])
                    {
                        aymin = plots[ch].y.a[pt];
                    }
                    if (aymax < plots[ch].y.a[pt])
                    {
                        aymax = plots[ch].y.a[pt];
                    }
                }

                if (!m_graph->xAutoScale())
                {
                    if (xmin>aymin)
                        aymin = xmin;
                    if (xmax<aymax)
                        aymax = xmax;
                }

                points = (aymax-aymin)/plots[ch].dx + 1;

                plots[ch].offs = aymin;

                mglData hist = input_plots[ch]->y.Hist(points, aymin, aymax+plots[ch].dx);
                plots[ch].y = hist;
            }
            else if (m_graph->xLogMode()) // && (plots[ch].offs <=0))
            {
                plots[ch].offs = plots[ch].dx/2;
                if (xmin < plots[ch].offs)
                    xmin = plots[ch].offs;
            }

        }
        else
        {
            show_plots[ch]=false;
        }
    }

    if (m_graph->xLogMode() && xmin <= 0) // && (plots[ch].offs <=0))
    {
        xmin = 1;
    }



    if (m_graph->xAutoScale() && ch_cnt)
    {
        double axmax = -RAND_MAX;
        double axmin = RAND_MAX;
        for (int ch=0; ch < ch_cnt; ch++)
        {
            if (show_plots[ch] && m_graph->plotIsEnabled(ch))
            {
                if (plots[ch].type == LPlotSet::LPLOT_TYPE_SEQ)
                {
                    double min = plots[ch].offs;
                    double max = plots[ch].offs + plots[ch].dx * (plots[ch].y.GetNx()-1);

                    if (mode==LQGraphWgt::MODE_HIST)
                    {
                        min-=plots[ch].dx;
                        max+=plots[ch].dx;
                    }

                    if (!ch || (max>axmax))
                        axmax = max;
                    if (!ch || (min<axmin))
                        axmin = min;
                }
                else
                {
                    for (int pt=0; pt < plots[ch].x.GetNx(); pt++)
                    {
                        if (axmin > plots[ch].x.a[pt])
                        {
                            axmin = plots[ch].x.a[pt];
                        }
                        if (axmax < plots[ch].x.a[pt])
                        {
                            axmax = plots[ch].x.a[pt];
                        }
                    }
                }
            }
        }

        if (axmax > axmin)
        {
            xmax = axmax;
            xmin = axmin;
        }
    }

    /* Если нужно отображать по оси X не весь график целиком, то
     * обрезаем массив в соответствии с размером отображения */
    for (int ch=0; ch < ch_cnt; ch++)
    {
        if (show_plots[ch] && m_graph->plotIsEnabled(ch))
        {
            /** @todo рассмотреть случай с LPLOT_TYPE_XY */
            if (plots[ch].type == LPlotSet::LPLOT_TYPE_SEQ)
            {
                int i1=0, i2 = plots[ch].y.GetNx()-1;
                bool crop = false;
                mreal val_max = plots[ch].offs + plots[ch].dx*i2;

                if (xmin > (plots[ch].offs + plots[ch].dx))
                {
                    i1 = (xmin - plots[ch].offs - plots[ch].dx)/plots[ch].dx;
                    crop = true;
                }

                if (xmax < (val_max-plots[ch].dx))
                {
                    i2 -= (val_max-plots[ch].dx-xmax)/plots[ch].dx;
                    crop = true;
                }

                if (crop && (i2 > i1))
                {
                    plots[ch].y.Crop(i1,i2);
                    plots[ch].offs += i1 * plots[ch].dx;
                }
            }
        }
    }


    if (m_graph->yAutoScale())
    {
        double aymax = -RAND_MAX;
        double aymin = RAND_MAX;
        for (int ch=0; ch < ch_cnt; ch++)
        {
            if (show_plots[ch] && m_graph->plotIsEnabled(ch))
            {
                for (int pt=0; pt < plots[ch].y.GetNx(); pt++)
                {
                    if (aymin > plots[ch].y.a[pt])
                    {
                        aymin = plots[ch].y.a[pt];
                    }
                    if (aymax < plots[ch].y.a[pt])
                    {
                        aymax = plots[ch].y.a[pt];
                    }
                }
            }
        }

        if (m_graph->yAutoScale() && (aymax > aymin))
        {
            ymax = aymax;
            ymin = aymin;
        }
    }

    if (!ymin && !ymax)
        ymax = RAND_MAX;




    gr->Alpha(true);
    gr->Light(true);
    gr->SetCut(mode != LQGraphWgt::MODE_HIST);
    gr->SetRanges(xmin, xmax, ymin, ymax);
    gr->SetRotatedText(false);

    double yshift = 0.1 * height/400;
    if (yshift < 0.03)
        yshift = 0.03;
    gr->SetTickShift(mglPoint(0.1, yshift));


    char format[20];
    int prec;
    if (!m_graph->xLogMode())
    {
        prec = -log10(xmax-xmin)+3;
        if (prec < 0)
            prec = 0;
        sprintf(format, "%%.%df", prec);
    }


    gr->SetTickTempl('x', format);


    prec = -log10(ymax-ymin)+3;
    if (prec < 0)
        prec = 0;
    sprintf(format, "%%.%df", prec);
    gr->SetTickTempl('y', format);



    gr->SetTickLen(-0.01, 1);

    //gr->SetTicksTime('x', 0, "1");
    //gr->Box();
    if (!m_graph->xLogMode())
    {
        gr->SetTicks('x', xstep);
    }
    gr->SetTicks('y', ystep);
    gr->SetFontSize(2.5*400/(height < 1 ? 1 : height-1));
    gr->SetTuneTicks(true);
    gr->SetTickSkip(true);

    double yzoom = 0.1*(height-100)/300 ;
    if (yzoom < 0)
        yzoom = 0;
    if (yzoom > 0.15)
        yzoom = 0.15;
    double xzoom = 0.1*(width+300)/1300;
    if (xzoom < 0)
        xzoom = 0;
    if (xzoom > 0.19)
        xzoom = 0.19;
    gr->Zoom(xzoom, yzoom,0.85, 1-xzoom/1.1);

    gr->Grid("xy", "h;");


    if (m_graph->xLogMode())
    {
        gr->SetFunc("lg(x)", "");
    }


    gr->Axis();//"xy-", "2k-");





    for (int ch=0; ch < ch_cnt; ch++)
    {
        if (show_plots[ch] && m_graph->plotIsEnabled(ch))
        {
            char pen[128];
            int val = m_graph->plotColor(ch).rgba();
            val = (val << 8) | ((val >> 24) & 0xFF);
            sprintf(pen, "{x%08X}",val);


            if (plots[ch].type == LPlotSet::LPLOT_TYPE_SEQ)
            {

                char opt[128];
                double start_pt =  plots[ch].offs;//
                double last_pt = plots[ch].offs+plots[ch].dx * (plots[ch].y.GetNx()-1);//-0.5);

                if (mode==LQGraphWgt::MODE_HIST)
                {
                    start_pt -= 0.5*plots[ch].dx;
                    last_pt += 0.5*plots[ch].dx;
                }

                sprintf(opt, "xrange %f %f", start_pt, last_pt);



                mglData* dat = 0;
                if (!m_graph->xLogMode())
                {
                    dat = show_dec(&plots[ch], xmin, xmax, m_graph->showPoints());
                    if (dat)
                    {
                        plots[ch].y = dat;
                    }
                }

                if (mode==LQGraphWgt::MODE_HIST)
                {
                    gr->Bars(plots[ch].y, pen, opt);
                    plots[ch].y.Crop(0, 0);
                }
                else
                {
                    gr->Plot(plots[ch].y, pen, opt);
                }

                delete dat;
            }
            else
            {
                gr->Plot(plots[ch].x, plots[ch].y, pen);
            }
        }
    }

    char *img_buf = new char[width*height*4];
    gr->GetRGBA(img_buf, width*height*4);
    delete gr;

    for (register long i=0; i < width*height;i++)
    {
        register uchar tmp;
        tmp = img_buf[4*i];
        img_buf[4*i] = img_buf[4*i+2];
        img_buf[4*i+2] = tmp;
    }

    for (int ch =0; ch < ch_cnt; ch++)
    {
        if (input_plots[ch])
            input_plots[ch]->ref_cnt--;
    }


    emit printDone((const unsigned char*)img_buf, width, height);

    delete[] plots;
    delete[] input_plots;
}
