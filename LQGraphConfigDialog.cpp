#include "LQGraphConfigDialog.h"
#include "ui_LQGraphConfigDialog.h"
#include "LQGraphWgt.h"

LQGraphConfigDialog::LQGraphConfigDialog(LQGraphWgt *graph) :
    QDialog(graph),
    ui(new Ui::LQGraphConfigDialog), m_graph(graph)
{
    ui->setupUi(this);


    ui->XScaleAuto->setChecked(graph->xAutoScale());
    ui->XMin->setValue(graph->xmin());
    ui->XMax->setValue(graph->xmax());

    ui->XLogMode->setChecked(graph->xLogMode());
    ui->XLogMode->setVisible(graph->xLogModeEnabled());

    ui->YScaleAuto->setChecked(graph->yAutoScale());
    ui->YMin->setValue(graph->ymin());
    ui->YMax->setValue(graph->ymax());
}

LQGraphConfigDialog::~LQGraphConfigDialog()
{
    delete ui;
}

void LQGraphConfigDialog::accept()
{

    m_graph->setXAutoScale(ui->XScaleAuto->isChecked());
    m_graph->setXScale(ui->XMin->value(), ui->XMax->value());

    m_graph->setYAutoScale(ui->YScaleAuto->isChecked());
    m_graph->setYScale(ui->YMin->value(), ui->YMax->value());

    m_graph->setXLogMode(ui->XLogMode->isChecked());

    m_graph->graphUpdate();

    QDialog::accept();
}

