#ifndef SIGNALGRAPH_H
#define SIGNALGRAPH_H

#include <QFrame>
#include <QMutex>

class QPaintEvent;
class QPixmap;
class LQGraphWgtPrint;
class QContextMenuEvent;
class QSettings;


class LPlotSet;

#ifdef _WIN32
    #ifdef LQGRAPH_WIDGET_EXPORTS
        #define LQGRAPHWIDGET_EXPORT   __declspec(dllexport)
    #else
        #define LQGRAPHWIDGET_EXPORT   __declspec(dllimport)
    #endif
#else
    #define LQGRAPHWIDGET_EXPORT
#endif


class LQGRAPHWIDGET_EXPORT LQGraphWgt : public QFrame
{
    Q_OBJECT
public:
    typedef enum
    {
        MODE_LINEAR = 0,
        MODE_HIST   = 1
    } GraphMode;


    explicit LQGraphWgt(QString name=QString(), QWidget *parent = 0, GraphMode mode = MODE_LINEAR, int max_plots=16);
    ~LQGraphWgt();

    double xmin() const {return m_xmin;}
    double xmax() const {return m_xmax;}
    double ymin() const {return m_ymin;}
    double ymax() const {return m_ymax;}
    double ystep() const {return m_ystep;}
    double xstep() const {return m_xstep;}
    bool xAutoScale() const {return m_xauto;}
    bool yAutoScale() const {return m_yauto;}
    bool xLogMode() const {return m_xLogMode;}
    bool xLogModeEnabled() const {return m_xLogModeEn;}



    QColor plotColor(int id) const;
    void setPlotColor(int id, QColor color);
    bool plotIsEnabled(int id) const;
    void plotEnable(int id, bool enable);


    void setShowPointsCnt(int points) {m_show_points = points;}
    int showPoints() const {return m_show_points;}


    int maxPlotCount() const;

    GraphMode mode() const {return m_mode;}
    QString name() const {return m_name;}


    virtual QSize sizeHint () const;


public slots:

    void setYScale(double ymin, double ymax);
    void setYAutoScale(bool autoScale);
    void setXScale(double xmin, double xmax);
    void setXAutoScale(bool autoScale);


    void setYGridStep(double ystep) {m_ystep = ystep;}
    void setXGridStep(double xstep) {m_xstep = xstep;}

    void setXLogMode(bool logMode) {m_xLogMode = logMode;}
    void setXLogModeEnabled(bool en) {m_xLogModeEn=en;}


    void plotData(int plot_id, const double* y, int size, double dx=1., double xstart=0.);
    void plotXY(int plot_id, const double* x, const double *y, int size);
    void clearPlot(int plot_id);
    void graphUpdate(void);

    void saveSettings(QSettings& set, QString groupName);
    void loadSettings(QSettings& set, QString groupName);
protected:
    void paintEvent (QPaintEvent * event );
    void mouseDoubleClickEvent (QMouseEvent * event);
    //void contextMenuEvent ( QContextMenuEvent * event );
signals:
    /* сигнал потоку формирования изображения на начало отрисовки */
    void startPlot(int width, int height, int ch_cnt, LPlotSet** data);
    /* сигнал потоку формирования изображения на завершение работы */
    void stopPlotThread(void);

    void yScaleChanged(double ymin, double ymax);
    void yAutoScaleChanged(bool autoScale);
    void xScaleChanged(double ymin, double ymax);
    void xAutoScaleChanged(bool autoScale);

private slots:
    /* слот вызывается потоком отрисовки по завершению формирования картинки */
    void setImage(const unsigned char *img_buf, int width, int height);

private:
    void clearOldPlots();


    struct LPlotCfg
    {
        bool en;
        QColor color;
        QString name;
    };

    QList<LPlotCfg> m_plotCfgs;
    GraphMode m_mode;
    QString m_name;

    void p_plot(void);
    LPlotSet **m_data;
    int m_size;
    int m_ch_cnt;
    int m_show_points;

    double m_xmin, m_xmax;
    double m_ymin, m_ymax;
    double m_xstep, m_ystep;
    bool m_xauto, m_yauto;
    bool m_xLogMode, m_xLogModeEn;


    QPixmap m_pict;
    LQGraphWgtPrint* m_printThread;
    int m_height, m_width;
    bool m_print_progress;
    bool m_print_deferred;
    QList<LPlotSet*> old_data_list;
    QMutex m_lock;
};

#endif // SIGNALGRAPH_H
