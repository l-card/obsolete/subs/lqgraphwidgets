#ifndef LQGRAPHCONFIGDIALOG_H
#define LQGRAPHCONFIGDIALOG_H

#include <QDialog>

class LQGraphWgt;

namespace Ui {
class LQGraphConfigDialog;
}

class LQGraphConfigDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit LQGraphConfigDialog(LQGraphWgt *graph);
    ~LQGraphConfigDialog();

public slots:
    virtual void accept();
    
private:
    Ui::LQGraphConfigDialog *ui;
    LQGraphWgt* m_graph;
};

#endif // LQGRAPHCONFIGDIALOG_H
