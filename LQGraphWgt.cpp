#include "LQGraphWgt.h"
#include "LQGraphWgtPrint.h"
#include "LPlotSet.h"
#include "LQGraphConfigDialog.h"

//#define NO_GSL
#include "mgl2/mgl.h"

#include <QPaintEvent>
#include <QPixmap>
#include <QPainter>
#include <QImage>
#include <QObject>
#include <QSettings>


static QColor f_colors[] = {Qt::blue, Qt::red, Qt::green, Qt::magenta,
                            Qt::darkCyan, Qt::yellow, Qt::darkGreen, Qt::black};

#define COLORS_CNT (sizeof(f_colors)/sizeof(f_colors[0]))

LQGraphWgt::LQGraphWgt(QString name, QWidget *parent, GraphMode mode, int max_plots) :
  QFrame(parent), m_height(0), m_width(0),
  m_print_progress(false), m_print_deferred(false), m_size(0),
   m_xmin(0), m_xmax(1000), m_ymin(0), m_ymax(10),
   m_xstep(0), m_ystep(0), m_ch_cnt(0), m_xauto(true), m_yauto(false), m_show_points(2000),
   m_mode(mode), m_name(name), m_xLogMode(false), m_xLogModeEn(false)

{
    this->setObjectName(name);
    /* настраиваем параметры рамки */
    this->setFrameStyle(QFrame::Panel | QFrame::Raised);
    this->setLineWidth(2);
    /* минимальный размер виджета */
    this->setMinimumSize(100,100);

    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    m_data = new LPlotSet*[max_plots];

    for (int ch =0; ch < max_plots; ch++)
    {
        m_data[ch] = NULL;
        LPlotCfg cfg;
        cfg.en = true;
        cfg.color = f_colors[ch%COLORS_CNT];
        m_plotCfgs << cfg;
    }


    /* запускаем фоновый поток для прорисвки */
    m_printThread = new LQGraphWgtPrint(this);
    m_printThread->start();
    m_printThread->moveToThread(m_printThread);

    /* соединяем сигналы-слоты для взаимодействия с фоновым потоком */
    connect(this, SIGNAL(startPlot(int,int,int, LPlotSet**)),
            m_printThread, SLOT(plot(int,int,int, LPlotSet**)));
    connect(m_printThread, SIGNAL(printDone(const unsigned char*,int,int)),
            SLOT(setImage(const unsigned char*,int,int)));
    connect(this, SIGNAL(stopPlotThread()), m_printThread, SLOT(stop()));

    //setContextMenuPolicy(Qt::CustomContextMenu);
    //connect(this, SIGNAL(customContextMenuRequested(QPoint)), SLOT(showContextMenu(QPoint)));
}

LQGraphWgt::~LQGraphWgt()
{
    /* шлем сигнал для завершения потока отрисовки и ждем его завершения */
    emit stopPlotThread();
    m_printThread->wait();
    /* удаляем поток и данные */
    delete m_printThread;

    clearOldPlots();

    for (int i=0; i<m_ch_cnt; i++)
        delete m_data[i];
}

void LQGraphWgt::setYScale(double ymin, double ymax)
{
    bool trig = (m_ymin!=ymin) || (m_ymax != ymax);
    m_ymin=ymin; m_ymax = ymax;
    if (trig)
        emit yScaleChanged(ymin, ymax);
}

void LQGraphWgt::setYAutoScale(bool autoScale)
{
    bool trig=m_yauto != autoScale;
    m_yauto = autoScale;
    if (trig)
        emit yAutoScaleChanged(autoScale);
}

void LQGraphWgt::setXScale(double xmin, double xmax)
{
    bool trig = (m_xmin!=xmin) || (m_xmax != xmax);
    m_xmin=xmin; m_xmax = xmax;
    if (trig)
        emit xScaleChanged(xmin, xmax);
}

void LQGraphWgt::setXAutoScale(bool autoScale)
{
    bool trig=m_xauto != autoScale;
    m_xauto = autoScale;
    if (trig)
        emit xAutoScaleChanged(autoScale);
}

QColor LQGraphWgt::plotColor(int id) const
{
    return id < m_plotCfgs.size() ? m_plotCfgs[id].color : QColor::Invalid;
}

void LQGraphWgt::setPlotColor(int id, QColor color)
{
    if (id<m_plotCfgs.size())
        m_plotCfgs[id].color = color;
}

bool LQGraphWgt::plotIsEnabled(int id) const
{
    return id < m_plotCfgs.size() ? m_plotCfgs[id].en : false;
}

void LQGraphWgt::plotEnable(int id, bool enable)
{
    if (id<m_plotCfgs.size())
        m_plotCfgs[id].en = enable;
}

int LQGraphWgt::maxPlotCount() const
{
    return m_plotCfgs.size();
}


QSize LQGraphWgt::sizeHint() const
{
    return QSize(1000, 1000);
}



void LQGraphWgt::setImage(const unsigned char* img_buf, int width, int height)
{
    m_pict = QPixmap::fromImage(QImage(img_buf, width, height, QImage::Format_ARGB32));
    delete img_buf;

    m_print_progress = false;
    if (m_print_deferred)
    {
        m_print_deferred = false;
        p_plot();
    }
    this->update();
}

void LQGraphWgt::clearOldPlots()
{
    for (int plot = old_data_list.size()-1; plot>=0; plot--)
    {
        LPlotSet* old_set = old_data_list.at(plot);
        if (old_set->ref_cnt==0)
        {
            old_data_list.removeAt(plot);
            delete old_set;
        }
    }
}



void LQGraphWgt::plotData(int plot_id, const double *y, int size, double dx, double xstart)
{
    m_lock.lock();

    if (m_ch_cnt<=plot_id)
        m_ch_cnt = plot_id + 1;

    if (m_data[plot_id])
    {
        old_data_list << m_data[plot_id];
    }
    m_data[plot_id] = new LPlotSet();
    m_data[plot_id]->dx = dx;
    m_data[plot_id]->offs = xstart;
    m_data[plot_id]->y.Set(y, size);
    m_data[plot_id]->type = LPlotSet::LPLOT_TYPE_SEQ;
    m_data[plot_id]->ref_cnt = 0;

    clearOldPlots();

    m_lock.unlock();
}

void LQGraphWgt::plotXY(int plot_id, const double *x, const double *y, int size)
{
    m_lock.lock();

    if (m_ch_cnt<=plot_id)
        m_ch_cnt = plot_id + 1;

    if (m_data[plot_id])
    {
        old_data_list << m_data[plot_id];
    }

    m_data[plot_id] = new LPlotSet();
    m_data[plot_id]->dx = 0;
    m_data[plot_id]->offs = 0;
    m_data[plot_id]->x.Set(x, size);
    m_data[plot_id]->y.Set(y, size);
    m_data[plot_id]->type = LPlotSet::LPLOT_TYPE_XY;
    m_data[plot_id]->ref_cnt = 0;

    clearOldPlots();

    m_lock.unlock();
}

void LQGraphWgt::clearPlot(int plot_id)
{
    m_lock.lock();

    if (m_ch_cnt<=plot_id)
        m_ch_cnt = plot_id + 1;

    if (m_data[plot_id])
    {
        old_data_list << m_data[plot_id];
    }
    m_data[plot_id] = 0;

    clearOldPlots();

    m_lock.unlock();
}

void LQGraphWgt::graphUpdate()
{
    p_plot();
    this->update();
}



void LQGraphWgt::p_plot(void)
{
    if (m_print_progress)
    {
        m_print_deferred = true;
    }
    else
    {
        m_print_progress = true;
        m_lock.lock();

        LPlotSet **snd_data = new LPlotSet*[m_ch_cnt];

        for (int ch=0; ch < m_ch_cnt; ch++)
        {
            if (m_data[ch])
            {
                snd_data[ch] = m_data[ch];
                snd_data[ch]->ref_cnt++;
            }
            else
            {
                snd_data[ch]=0;
            }
        }
        emit startPlot(this->width(), this->height(), m_ch_cnt, snd_data);

        clearOldPlots();

        m_lock.unlock();
    }
}


void LQGraphWgt::paintEvent (QPaintEvent * event )
{
    QFrame::paintEvent(event);

    if ((this->width()!=m_width) || (this->height()!= m_height))
    {
        m_width = this->width();
        m_height = this->height();
        p_plot();
    }

    QPainter paint;
    paint.begin(this);  paint.drawPixmap(0,0,m_pict);  paint.end();
}

void LQGraphWgt::mouseDoubleClickEvent(QMouseEvent *event)
{
    LQGraphConfigDialog dialog(this);
    dialog.exec();
}


void LQGraphWgt::saveSettings(QSettings &set, QString groupName)
{
    set.beginGroup(groupName);
    set.beginGroup("X");
    set.setValue("auto_scale", m_xauto);
    set.setValue("min", m_xmin);
    set.setValue("max", m_xmax);
    set.endGroup();

    set.beginGroup("Y");
    set.setValue("auto_scale", m_yauto);
    set.setValue("min", m_ymin);
    set.setValue("max", m_ymax);
    set.endGroup();


    for (int ch=0; ch < maxPlotCount(); ch++)
    {
        set.beginGroup("plot_" + QString::number(ch));
        set.setValue("color", m_plotCfgs[ch].color);
        set.setValue("name", m_plotCfgs[ch].name);
        set.setValue("enabled", m_plotCfgs[ch].en);
        set.endGroup();
    }


    set.endGroup();
}

void LQGraphWgt::loadSettings(QSettings &set, QString groupName)
{
    set.beginGroup(groupName);
    set.beginGroup("X");
    setXAutoScale(set.value("auto_scale", m_xauto).toBool());
    setXScale(set.value("min", m_xmin).toDouble(), set.value("max", m_xmax).toDouble());
    set.endGroup();

    set.beginGroup("Y");
    setYAutoScale(set.value("auto_scale", m_yauto).toBool());
    setYScale(set.value("min", m_ymin).toDouble(), set.value("max", m_ymax).toDouble());
    set.endGroup();

    for (int ch=0; ch < maxPlotCount(); ch++)
    {
        set.beginGroup("plot_" + QString::number(ch));
        m_plotCfgs[ch].color = set.value("color", m_plotCfgs[ch].color).value<QColor>();
        m_plotCfgs[ch].name = set.value("name").toString();
        m_plotCfgs[ch].en = set.value("enabled", true).toBool();
        set.endGroup();
    }
    set.endGroup();
}
